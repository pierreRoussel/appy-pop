import React, { useEffect } from 'react';
import useLocalStorage from 'use-local-storage';
import ThemeButtonComponent from './components/themeButton/themebutton.component';
import './App.scss';
import HomePage from './components/homePage/home.page';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CartodexPage from './components/cartodexPage/cartodex.page';
import CardChoicePage from './components/cardChoicePage/cardchoice.page';
import CardReadPage from './components/cardReadPage/cardread.page';
import { getUser, setUserLimitcard, setTimestamp, setUserTheme } from './services/user.service';
import { Timestamp } from 'firebase/firestore';
import DashboardPage from './components/backoffice/dashboardPage/dashboard.page';
import CreateCardPage from './components/backoffice/createCardPage/create-card.page';

function App() {
  const defaultDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
  const [theme, setTheme] = useLocalStorage('theme', defaultDark ? true : false);

  const switchTheme = () => {
    const newTheme = !theme;
    setUserTheme(newTheme)
    setTheme(newTheme);
  }

  const checkTimestamp = (timestamp: Timestamp) => {
    const actualDate = new Date();
    const timeStampToDate = timestamp.toDate();
    // if only date without hours is anterior, update limit card and last connection timestamp
    if (timeStampToDate.setHours(0, 0, 0, 0) < actualDate.setHours(0, 0, 0, 0)) {
      setUserLimitcard(3);
      setTimestamp(Timestamp.fromDate(actualDate));
    };
  }

  useEffect(() => {
    getUser()
      .then((userRes) => {
        checkTimestamp(userRes.lastConnection);
        setTheme(userRes.theme);
      })
  }, [])

  return (
    <BrowserRouter>
      <ThemeButtonComponent theme={theme ? 'light' : 'dark'} setter={switchTheme} />
      <div className="App" data-theme={theme ? 'light' : 'dark'}>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="cartodex" element={<CartodexPage />} />
          <Route path="cardchoice" element={<CardChoicePage />} />
          <Route path="cardread/:category" element={<CardReadPage />} />
          <Route path="backoffice" element={<DashboardPage />}>
            <Route path="create" element={<CreateCardPage />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
