import React from 'react'
import { Link } from 'react-router-dom';
import './basic-diamond-btn.component.scss';

type BasicDiamondBtnComponentProps = {
    callback?: Function,
    img: string,
    color: string
}

export default function BasicDiamondBtnComponent(props: BasicDiamondBtnComponentProps) {
    return (
        <div onClick={(e) => props.callback && props.callback()} className={`basic-diamond-btn__container ${props.color}-bg`}>
            <img src={require(`../../images/${props.img}.png`)} alt={`${props.img}`} className="basic-diamond-btn__img" />
        </div>
    )
}
