import React from 'react'
import { useNavigate } from 'react-router-dom';
import BasicCloudLinkComponent from '../basicCloudLink/basic-cloud-link.component';
import './basic-card-btn.component.scss';

type BasicCardBtnComponentProps = {
    img: string,
    text: string,
    link: string,
    counter?: number,
    reverse?: boolean,
    linkColor?: string,
    disabled?: boolean
}

export default function BasicCardBtnComponent(props: BasicCardBtnComponentProps) {
    let navigate = useNavigate();

    const handleClick = (e: React.MouseEvent) => {
        props.disabled ? e.preventDefault() : navigate(props.link)
    }

    return (
        <div onClick={(e) => handleClick(e)} className={`basic-card-btn__container ${props.reverse ? 'reverse' : ''} ${props.disabled ? 'link-disabled' : ''}`}>
            <img src={require(`../../images/${props.img}.png`)} alt={`${props.img}`} className="basic-card-btn__img" />
            <BasicCloudLinkComponent link={props.disabled ? '' : props.link} text={props.text} color={props.linkColor} />
        </div>
    )
}
