import React from 'react';
import { Link } from 'react-router-dom';
import './basic-cloud-link.component.scss';

type BasicCloudLinkComponentType = {
    link: string,
    text: string,
    color?: string
}

export default function BasicCloudLinkComponent(props: BasicCloudLinkComponentType) {
    return (
        <Link to={props.link} className="cloud-link__container">
            <span className='cloud-link__text'>{props.text}</span>
            <svg className='cloud-link__cloud' xmlns="http://www.w3.org/2000/svg"
                width="200px" height="80px"
                viewBox="0 0 458 279">
                <path id="cloud"
                    fill={props.color || "#fbb957"} stroke="none" strokeWidth="1"
                    d="M 43.00,108.00
           C 43.00,108.00 49.67,93.00 117.67,90.67
             133.33,76.67 156.00,67.67 171.00,67.00
             186.00,66.33 236.67,63.33 281.33,83.00
             332.33,79.00 388.67,73.33 407.67,107.33
             407.33,117.00 405.00,119.33 392.33,129.33
             412.33,138.67 428.33,144.67 428.67,157.00
             426.67,163.00 425.33,174.00 358.67,176.67
             344.33,184.67 238.67,227.00 144.67,177.33
             128.33,179.00 75.33,187.33 52.00,158.67
             51.67,153.67 53.45,143.23 77.78,138.57
             71.78,136.57 47.67,133.67 39.67,123.67
             35.83,115.52 39.94,111.23 43.11,107.80" />
            </svg>
        </Link>
    )
}
