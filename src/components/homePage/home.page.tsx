import React from 'react'
import BasicCardBtnComponent from '../basicCardBtn/basic-card-btn.component'
import './home.page.scss'

export default function HomePage() {
    return (
        <div className='home__container'>
            <h1>Appy... pop !</h1>
            <div className="home__img-container">
                <img src={require("../../images/fox.png")} alt="fox" className='fox' />
                <img src={require("../../images/leafs.png")} alt="feuilles" className="leafs" />
                <img src={require("../../images/wind.png")} alt="" className="left-wind" />
                <img src={require("../../images/wind.png")} alt="" className="right-wind" />
            </div>
            <div className="home__welcome-text-container">
                <h2>Bienvenue</h2>
                <p>On fait quoi aujourd'hui petit renard ?</p>
            </div>
            <div className="home__cards-container">
                <BasicCardBtnComponent link="cartodex" img="cat-painting" text="Envie de consulter le cartodex ?" />
                <BasicCardBtnComponent link="cardchoice" img="looking-love" text="Lis une nouvelle carte !" linkColor='#ff8d83' />
            </div>
        </div>
    )
}
