import React, { useEffect, useRef, useState } from 'react';
import { Card, Category, getAllReadenCards, Reaction } from '../../services/card.service';
import BasicCloudLinkComponent from '../basicCloudLink/basic-cloud-link.component';
import CardComponent from './cardComponent/card.component';
import './cartodex.page.scss';

enum Side {
    "left" = -1,
    "right" = 1
}

const emptyCard = {
    id: "string",
    category: Category.inmymind,
    read: true,
    text: "Oups... on dirait que tu n'as pas encore regardé de carte ! Essaie d'aller en tirer et reviens :D",
    reaction: Reaction.inspiree
} as Card;

export default function CartodexPage() {
    const [items, setItems] = useState([] as Card[]);
    const [visibleItems, setVisibleItems] = useState([] as Card[]);
    const scrollRef = useRef(null);
    const [firstItemVisible, setFirstItemVisible] = useState(false);
    const [lastItemVisible, setLastItemVisible] = useState(true);
    const [anchors, setAnchors] = useState([0]);
    const [actualFilter, setActualFilter] = useState(0);

    function timeout(delay: number) {
        return new Promise(res => setTimeout(res, delay));
    }

    const getAnchors = () => {
        let newAnchors = [0];
        for (let i = 1; i < items.length; i++) {
            newAnchors = ([...newAnchors, (+newAnchors[newAnchors.length - 1] + 350)]);
        }
        setAnchors(newAnchors);
    };

    const handleScroll = () => {
        if (scrollRef?.current) {
            setFirstItemVisible((scrollRef?.current?.["scrollLeft"] - 150) >= 0);
            setLastItemVisible(scrollRef?.current?.["scrollLeft"] + 500 <= scrollRef?.current?.["scrollWidth"]);
        }
    }

    const getNextCard = (side: Side) => {
        let closest = [0, 350];
        if (scrollRef?.current) {
            const currAnchors = [...anchors];
            const currRef = scrollRef?.current;
            closest = currAnchors.sort((a, b) =>
                Math.abs(currRef?.['scrollLeft'] - a)
                - Math.abs(currRef?.['scrollLeft'] - b));
        }
        return anchors[anchors.indexOf(closest[0]) + (side)];
    }

    const scrollTo = async (side: Side) => {
        let nextStep = getNextCard(side);
        if (scrollRef.current) {
            let stopper = false;
            while (!stopper) {
                if (side === Side.left) {
                    scrollRef.current?.['scrollLeft'] > nextStep
                        //@ts-ignore
                        ? scrollRef.current.scrollLeft -= 10
                        : stopper = true
                } else {
                    scrollRef.current?.['scrollLeft'] < nextStep
                        //@ts-ignore
                        ? scrollRef.current.scrollLeft += 10
                        : stopper = true
                }
                await timeout(0.2);
            }
        }
    }

    const filter = (reaction: Reaction) => {
        if (actualFilter === reaction) {
            setVisibleItems(items)
            setActualFilter(0);
        } else {
            setVisibleItems(items.filter((item: Card) => item.reaction === reaction));
            setActualFilter(reaction);
        }
        handleScroll()
    }

    useEffect(() => {
        if (visibleItems.length === 0) {
            getAllReadenCards().then((cardsRes: Card[]) => {
                if (cardsRes.length > 0) {
                    setItems(cardsRes);
                    setVisibleItems(cardsRes)
                    getAnchors()
                } else {
                    setVisibleItems([emptyCard])
                }
            })
        }
    })

    return (
        <div className='cartodex__container'>
            <div className="cartodex__header-container">
                <BasicCloudLinkComponent link='/' text="Retour" />
                <h1><i>Ton cartodex</i></h1>
            </div>
            <div className={`cartodex__inner-container`}>
                <div className="inner__header">
                    <h2>Filtrer</h2>
                    <div className={`filter-btn__container ${(actualFilter === 1) && 'active'}`} onClick={() => filter(1)}>
                        <img src={require(`../../images/easel.png`)} alt={`easel`} className="basic-diamond-btn__img" />
                    </div>
                    <div className={`filter-btn__container ${(actualFilter === 2) && 'active'}`} onClick={() => filter(2)}>
                        <img src={require(`../../images/bee.png`)} alt={`bee`} className="basic-diamond-btn__img" />
                    </div>
                    <div className={`filter-btn__container ${(actualFilter === 3) && 'active'}`} onClick={() => filter(3)}>
                        <img src={require(`../../images/leaf.png`)} alt={`leaf`} className="basic-diamond-btn__img" />
                    </div>
                </div>
                <div className="inner__content" ref={scrollRef} onScroll={() => handleScroll()}>
                    {visibleItems.map((item: Card) => {
                        return <CardComponent {...item} key={item.id}></CardComponent>
                    })}
                </div>
                <div className="content-arrow__container">
                    {
                        firstItemVisible &&
                        <div className="arrow arrow-left" onClick={() => scrollTo(Side.left)}></div>
                    }
                    {
                        lastItemVisible &&
                        <div className="arrow arrow-right" onClick={() => scrollTo(Side.right)}></div>

                    }
                </div>
                <div className="bottom__container">
                    <p>{items.length} cartes collectionnées !</p>
                </div>
            </div>

        </div>
    )
}