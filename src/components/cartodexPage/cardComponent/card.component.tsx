import React from 'react';
import { Card, Reaction } from '../../../services/card.service';
import './card.component.scss';

export default function CardComponent(card: Card) {
    return (
        <div className="content__card-container">
            <div className={`card ${Reaction[card.reaction as Reaction]}`}>
                <div className='card__content'>
                    {card.text}
                </div>
            </div>
        </div>
    );
}