import React, { useEffect, useState } from 'react'
import { getLimitcard } from '../../services/user.service';
import BasicCardBtnComponent from '../basicCardBtn/basic-card-btn.component';
import BasicCloudLinkComponent from '../basicCloudLink/basic-cloud-link.component';
import './cardchoice.page.scss';

export default function CardChoicePage() {
    const [limitCard, setLimitCard] = useState(0);

    useEffect(() => {
        getLimitcard().then(limitCardRes => {
            setLimitCard(limitCardRes)
        })
    })

    return (
        <div className="card-choice__container">
            <div className="card-choice__header-container">
                <BasicCloudLinkComponent link='/' text="Retour" />
                <h1><i>On pioche ?</i></h1>
            </div>
            <div className="card-choice__main-text">
                Choisis une des deux cartes, je suis sûr que ça va te plaire !
            </div>
            <div className="card-choice__cards-container">
                <img className='card-choice__img' src={require("../../images/lovely-daisies.png")} alt="" />
                <div className="cards">
                    <div className="left">
                        <BasicCardBtnComponent disabled={limitCard < 1 && true} reverse={true} img="romantic-bear" text="Viens je t'emmène dans ma tête" link="/cardread/1" />
                    </div>
                    <div className="right">
                        <BasicCardBtnComponent disabled={limitCard < 1 && true} reverse={true} img="artsy-cat" text="Laisse moi te parler de toi" link="/cardread/2" linkColor='#ff8d83' />
                    </div>
                </div>
            </div>
            <div className="card-choice__bottom-container">
                <div className="counter"> <b>{limitCard}</b> / 3</div>
                <div className="text">
                    Tu as un nombre limité de cartes
                    par jour, mais pas d'inquiétude, ça se recharge
                    tous les jours !
                </div>
            </div>
        </div>
    )
}
