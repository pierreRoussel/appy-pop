import React, { useState } from 'react'
import { CardPostBody, createOnCard } from '../../../services/card.service';

export default function CreateCardPage() {
    const [category, setCategory] = useState(1);
    const [text, setText] = useState('');
    const [img, setImg] = useState('');

    const handleSubmit = async (e: any) => {
        e.preventDefault();
        const newCard: CardPostBody = {
            category,
            text,
            read: false,
            img
        }
        console.log("🚀 ~ newCard", newCard)
        createOnCard(newCard).then(res => {
            resetForm();
            alert("Carte enregistrée !")
        }).catch(err => {
            alert(err)
        })

    }

    const resetForm = () => {
        setText("");
        setImg("");
    }

    return (
        <form onSubmit={e => { handleSubmit(e) }}>
            <h2>
                Créer une carte
            </h2>
            <div className="card">
                <textarea
                    name="text"
                    id="text"
                    placeholder='texte de la carte'
                    onChange={(e) => setText(e.target.value)}
                />
            </div>
            <label htmlFor="image">Image</label>
            <input type="text" name="image" id="image" />
            <label htmlFor="category">Catégorie</label>
            <select name="category" id="category" onChange={(e) => setCategory(+e.target.value)}>
                <option value="1">In my mind</option>
                <option value="2">About you</option>
            </select>
            <input
                value={"Envoyer !"}
                type='submit'
            />
        </form>
    )
}
