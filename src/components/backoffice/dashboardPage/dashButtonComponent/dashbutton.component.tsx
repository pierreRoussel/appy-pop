import React from 'react'
import { Link } from 'react-router-dom';
import './dashbutton.component.scss';

type DashButtonProps = {
    color: string,
    link: string,
    img: string,
    text?: string
}

export default function DashbuttonComponent(props: DashButtonProps) {
    return (
        <Link to={`${props.link}`} className={`dashbutton__container ${props.color}`}>
            <img alt={`${props.img}`} src={require(`../../../../images/${props.img}.png`)}></img>
            <div className="dashbutton__text">
                {props.text || `Lien vers ${props.link}`}
            </div>
        </Link>
    )
}
