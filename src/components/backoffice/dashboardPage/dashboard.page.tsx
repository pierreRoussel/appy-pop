import React from 'react'
import DashbuttonComponent from './dashButtonComponent/dashbutton.component';
import "./dashboard.page.scss"
import { Outlet } from 'react-router';

export default function DashboardPage() {
    return (
        <div className='dashboard__container'>
            <div className="dashboard__header">
                <div className="header__logo">
                    <img src={require("../../../images/fox.png")}></img>
                </div>
                <div className="header__title">
                    <h1>Appy Pop Dashboard</h1>
                </div>
            </div>
            <div className="dashboard__inner">
                <div className="inner__buttons-container">
                    <DashbuttonComponent text='Créer une carte' color='primary-grdt' link='create' img='fox' />
                    <DashbuttonComponent text="Suivre l'activité" color='secondary-grdt' link='track' img='easel' />
                    <DashbuttonComponent text='Animer' color='calm-grdt' link='involve' img='bunny' />

                </div>
            </div>
            <div className="content__container">
                <Outlet />
            </div>
        </div>
    )
}
