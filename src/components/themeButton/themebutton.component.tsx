import React from 'react'
import './themebutton.component.scss';

type themeProps = {
    theme: string,
    setter: Function
}
export default function ThemeButtonComponent(props: themeProps) {
    return (
        <div className='theme-btn__container' onClick={() => props.setter()}>
            <img src={require(`../../images/${props.theme}.png`)} alt={`${props.theme}`} />
        </div>
    )
}
