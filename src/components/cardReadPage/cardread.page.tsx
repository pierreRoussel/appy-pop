import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import { Card, getOneUnreadCardByCategory, setOneCardReaction, setOneCardToRead } from '../../services/card.service'
import { getLimitcard, setUserLimitcard } from '../../services/user.service'
import BasicCloudLinkComponent from '../basicCloudLink/basic-cloud-link.component'
import BasicDiamondBtnComponent from '../basicDiamondBtn/basic-diamond-btn.component'
import './cardread.page.scss'

export default function CardReadPage() {
    let { category } = useParams();
    let [card, setCard] = useState({} as Card);
    let [loading, setLoading] = useState(false);
    let [style, setStyle] = useState("");
    let [isConfirmed, setIsConfirmed] = useState(0)

    const setCardToRead = (cardId: string) => {
        setOneCardToRead(cardId);
    }

    const setReaction = async (reactionId: number) => {
        setOneCardReaction(card.id, reactionId);
        setStyle("reaction step-1");
        await timeout(750);
        setStyle("reaction step-2");
        setIsConfirmed(reactionId);
    }

    function timeout(delay: number) {
        return new Promise(res => setTimeout(res, delay));
    }

    useEffect(() => {
        getOneUnreadCardByCategory(Number(category)).then((cardRes: Card | null) => {
            if (cardRes) {
                setCard(cardRes);
                setCardToRead(cardRes.id);
                getLimitcard().then(limitCardRes => {
                    setUserLimitcard(limitCardRes - 1)
                })
            }
            setLoading(false);
        })
    }, [])

    return (
        <div className={`card-read__container ${style}`}>
            <div className="card-read__header-container">
                <BasicCloudLinkComponent link='/cardchoice' text="Retour" />
                <h1><i>{(Number(category) === 1) ? "Viens je t'emmène dans ma tête" : "Un petit quelque chose pour toi"}</i></h1>
            </div>

            {!loading ?
                <div>
                    <div className="card-read__card-container">
                        {isConfirmed !== 0 &&
                            <div className="confirm-container">
                                {(isConfirmed === 1) &&
                                    <p>Plutôt <b><i className="secondary">inspirée</i></b> alors ? J'espère que cette carte t'a plu ! <br /> <br />
                                        Dans tous les cas, tu peux la retrouver dans ton <i>Cartodex</i> en cliquant sur le canevas !
                                        <br />
                                        <Link to="/cartodex">
                                            <img className='card__content-img' src={require("../../images/easel.png")} alt="" />
                                        </Link>
                                        <br />
                                        <br />
                                        <Link to="/cardchoice">
                                            Ou en lire une autre ... ?
                                        </Link>
                                    </p>
                                }
                                {isConfirmed === 2 &&
                                    <p>Oh... ça t'a <b><i className="primary">piqué</i></b> l'intérêt ? Garde la précieusement en réserve pour m'en parler... <br /> <br />
                                        Tu la retrouveras dans ton <i>Cartodex</i> juste en appuyant sur cette abeille <i>(elle ne pique même pas... !)</i>
                                        <br />
                                        <Link to="/cartodex">
                                            <img className='card__content-img' src={require("../../images/bee.png")} alt="" />
                                        </Link>
                                        <br />
                                        <br />
                                        <Link to="/cardchoice">
                                            Ou en lire une autre ... ?
                                        </Link>
                                    </p>
                                }
                                {isConfirmed === 3 &&
                                    <p>Si cette carte a pu t'<b><i className="calm">apaiser</i></b>, alors j'espère que tu pourras la relire à chaque fois que tu en auras besoin, elle est maintenant dans ton <i>Cartodex</i>. <br /> <br />
                                        Tu peux cliquer sur la feuille pour la retrouver tout de suite
                                        <br />
                                        <Link to="/cartodex">
                                            <img className='card__content-img' src={require("../../images/leaf.png")} alt="" />
                                        </Link>
                                        <br />
                                        <br />
                                        <Link to="/cardchoice">
                                            Ou en lire une autre ... ?
                                        </Link>
                                    </p>
                                }
                                <img src={require("../../images/balloon.png")} className="ballon b-1"></img>
                                <img src={require("../../images/balloon.png")} className="ballon b-2"></img>
                                <img src={require("../../images/balloon.png")} className="ballon b-3"></img>
                                <img src={require("../../images/balloon.png")} className="ballon b-4"></img>
                            </div>
                        }
                        <div className="card">
                            <div className='card__content'>
                                {card.img &&
                                    <div className="card__content-img-container">
                                        <img className='card__content-img' src={require(`../../images/${card.img}.png`)} alt="" />
                                    </div>
                                }
                                {!card.img && !card.text &&
                                    <div className="card__content-img-container">
                                        <img className='card__content-img' src={require("../../images/bunny.png")} alt="" />
                                    </div>}
                                {card.text ?
                                    card.text :
                                    <div>
                                        Oups... on dirait qu'il n'y plus de cartes dans ce deck là... Tu veux tester l'autre ?
                                        <br />
                                        <br />
                                        <br />
                                        <BasicCloudLinkComponent link={`/cardread/${(category && +category === 1) ? 2 : 1}`} text={"Oui !"} color={"#67ab9f"} />
                                    </div>

                                }
                            </div>
                        </div>
                        {card.id &&
                            <div className="btns__container">
                                <BasicDiamondBtnComponent callback={() => setReaction(1)} img='easel' color='secondary' />
                                <BasicDiamondBtnComponent callback={() => setReaction(2)} img='bee' color='primary' />
                                <BasicDiamondBtnComponent callback={() => setReaction(3)} img='leaf' color='calm' />
                            </div>
                        }
                    </div>
                    <div className="card-read__footer-container">
                        {card.id &&
                            <p>
                                Plutôt <i className='primary'>piquée d'intérêt </i> *bzz*?
                                <i className="secondary"> Inspirée </i> ou <i className="calm"> apaisée </i>?
                            </p>
                        }
                    </div>
                </div>
                :
                <div>
                    Attention ça arrive ...! (attends encore un peu)
                </div>
            }
        </div>
    )
}
