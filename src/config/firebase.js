import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyDUCp0-NpDR6Z-0TO01gUw_ja1L7spCyRY",
    authDomain: "appy-pop.firebaseapp.com",
    projectId: "appy-pop",
    storageBucket: "appy-pop.appspot.com",
    messagingSenderId: "1019893180119",
    appId: "1:1019893180119:web:aad0e20e236d08687ff292",
    measurementId: "G-HK40Y9GE2G"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;