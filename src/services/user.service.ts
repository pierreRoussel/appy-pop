import db from '../config/firebase';
import { doc, getDoc, Timestamp, updateDoc } from "firebase/firestore";

type User = {
    theme: boolean,
    lastConnection: Timestamp,
    limitCard: number
}

export const getUser = async () => {
    const res = await getDoc(doc(db, "user", "renard"));
    return res.data() as User
}

export const setUserTheme = async (theme: boolean) => {
    await updateDoc(doc(db, "user", "renard"), { theme: theme });
}

export const setUserLimitcard = async (limitCard: number) => {
    await updateDoc(doc(db, "user", "renard"), { limitCard: limitCard });
}

export const setTimestamp = async (timestamp: Timestamp) => {
    await updateDoc(doc(db, "user", "renard"), { lastConnection: timestamp });
}

export const getLimitcard = async () => {
    const res = await getDoc(doc(db, "user", "renard"));
    return res.data()?.limitCard || 0
}