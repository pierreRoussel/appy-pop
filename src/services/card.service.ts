import db from '../config/firebase';
import { collection, doc, getDocs, limit, query, setDoc, updateDoc, where } from "firebase/firestore";

export enum Reaction {
    "inspiree" = 1,
    "piquee" = 2,
    "apaisee" = 3
}

export enum Category {
    "inmymind" = 1,
    "aboutyou" = 2
}

export type Card = {
    id: string,
    category: Category,
    read: boolean,
    text: string,
    reaction?: Reaction,
    img?: string
}

export type CardPostBody = {
    category: Category,
    read: boolean,
    text: string,
    img?: string
}

const cardRef = collection(db, "card");

export const getOneUnreadCardByCategory = async (categoryId: number = 1) => {
    const q = query(cardRef,
        where("read", "==", false),
        where("category", "==", categoryId),
        limit(1));
    const res = await getDocs(q);
    return res.docs.length === 1 ? { id: res.docs[0].id, ...res.docs[0].data() } as Card : null
}

export const getAllReadenCards = async () => {
    const q = query(cardRef, where("read", "==", true));
    const res = await getDocs(q);
    return res.docs.map(data => {
        return { id: data.id, ...data.data() } as Card;
    });
}

export const setOneCardToRead = async (cardId: string) => {
    await updateDoc(doc(db, "card", cardId), { read: true });
}

export const setOneCardReaction = async (cardId: string, reaction: number) => {
    await updateDoc(doc(db, "card", cardId), { reaction: reaction });
}

export const createOnCard = async (card: CardPostBody) => {
    await setDoc(doc(cardRef), card);
}